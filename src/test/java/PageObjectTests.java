import org.testng.annotations.Test;
import ru.protei.models.User;
import ru.protei.pageObject.steps.BaseTest;

import static ru.protei.testData.Users.admin;
import static ru.protei.testData.Users.getUserRandomData;

public class PageObjectTests extends BaseTest {
    /**
     * Класс с тестами на архитектуре "Page Object".
     * Внимание! В боевом проекте не надо складывать все тесты в кучу.
     * Лучше сделать директории под макро-функционал (например, тут - Авторизация, Пользователи)
     * в которые добавить классы с отдельными фичами (например, позитивные и негативные тесты авторизации,
     * создание и просмотр пользователей, пользователь с урезанными правами)
     *
     * По-хорошему надо еще весь "обычный" текст убрать в файл с локалью, но этого не было в статье(
     */

    @Test
    public void authAsAdmin() {
        baseRouter.authPage()
                .fillEmail(admin.getMail())
                .fillPassword(admin.getPassword())
                .clickEnter()
                .shouldExistLogout();
        //это классический PageObj, поэтому метод входа возвращает главную страницу. Мне кажется, это запутывает)
    }

    @Test
    void addUser() {
        //Arrange
        User userForTest = getUserRandomData();
        baseRouter.authPage()
                .complexLogin(admin)
                .complexOpenAddUser()
                .complexAddUser(userForTest)
                .checkAndCloseSuccessfulAlert();
    }

    @Test
    void addUserWithoutComplex() {
        //Arrange
        User userForTest = getUserRandomData();
        baseRouter.authPage()
                .complexLogin(admin);
        baseRouter.mainPage()
                .hoverUsersOpener()
                .clickAddUserMenu();
        //Act
        baseRouter.usersPage()
                .sendKeysEmail(userForTest.getMail())
                .sendKeysPassword(userForTest.getPassword())
                .sendKeysName(userForTest.getName())
                .selectGender(userForTest.getGender().getVisibleText())
                .selectVar11(userForTest.isCheck11())
                .selectVar12(userForTest.isCheck12())
                .selectVar21(userForTest.isCheck21())
                .selectVar22(userForTest.isCheck22())
                .selectVar23(userForTest.isCheck23())
                .clickSave();
        //Assert
        baseRouter.usersPage()
                .checkTextSavePopup("Данные добавлены.")
                .closeSavePopup();
    }

    @Test
    void addUserAndCheckInTable() {
        //Arrange
        User userForTest = getUserRandomData();
        baseRouter.authPage().complexLogin(admin);
        baseRouter.mainPage().complexOpenAddUser();
        //Act
        baseRouter.usersPage()
                .sendKeysEmail(userForTest.getMail())
                .sendKeysPassword(userForTest.getPassword())
                .sendKeysName(userForTest.getName())
                .selectGender(userForTest.getGender().getVisibleText())
                .selectVar11(userForTest.isCheck11())
                .selectVar12(userForTest.isCheck12())
                .selectVar21(userForTest.isCheck21())
                .selectVar22(userForTest.isCheck22())
                .selectVar23(userForTest.isCheck23())
                .clickSave()
                .checkAndCloseSuccessfulAlert();
        baseRouter.mainPage()
                .hoverUsersOpener()
                .clickUsersMenu();
        //Assert
        baseRouter.usersPage()
                .checkEntityIsPresentUsersTable("E-Mail", userForTest.getMail())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Имя", userForTest.getName())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Пол", userForTest.getGender().getVisibleText())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Выбор 1", userForTest.getFirstChoice())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Выбор 2", userForTest.getSecondChoice());
    }

    @Test
    void checkUserVars() {
        //Arrange
        User userForTest = getUserRandomData();
        // Проверка корректности сохранения полей уже есть, этот тест проверяет отображение вариантов из-под юзера, поэтому не важно, как юзер создан
        usersSteps.createUser(userForTest);
        baseRouter.authPage().complexLogin(userForTest);
        //Act
        baseRouter.mainPage()
                .clickVariantsMenu();
        //Assert
        baseRouter.variantsPage()
                .checkAllVariantsArePresent(userForTest.getVars())
                .checkVariantsCount(userForTest.getVarsCount());
        //Cleanup
        usersSteps.deleteUser(userForTest);
    }
}
