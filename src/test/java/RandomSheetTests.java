import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

public class RandomSheetTests {
    /**
     * Класс с тестами, скинутыми в одну кучу.
     * Демо архитектуры "простыни".
     */

    @Test
    void authAsAdmin() {
        open("https://ui-app-for-autotest.herokuapp.com/");
        $("#loginEmail").sendKeys("test@protei.ru");
        $("#loginPassword").sendKeys("test");
        $("#authButton").click();
        $("#menuMain").shouldBe(Condition.appear);
        WebDriverRunner.closeWebDriver();
    }

    @Test
    void addUser() {
        open("https://ui-app-for-autotest.herokuapp.com/");
        $("#loginEmail").sendKeys("test@protei.ru");
        $("#loginPassword").sendKeys("test");
        $("#authButton").click();
        $("#menuMain").shouldBe(Condition.appear);

        $("#menuUsersOpener").hover();
        $("#menuUserAdd").click();

        $("#dataEmail").sendKeys("mail@mail.ru");
        $("#dataPassword").sendKeys("testPassword");
        $("#dataName").sendKeys("testUser");
        $("#dataGender").selectOptionContainingText("Женский");
        $("#dataSelect12").click();
        $("#dataSelect21").click();
        $("#dataSelect22").click();
        $("#dataSend").click();

        $(".uk-modal-body").shouldHave(Condition.text("Данные добавлены."));

        WebDriverRunner.closeWebDriver();
    }

    @Test
    void addUserAndCheckInTable() {
        open("https://ui-app-for-autotest.herokuapp.com/");
        $("#loginEmail").sendKeys("test@protei.ru");
        $("#loginPassword").sendKeys("test");
        $("#authButton").click();
        $("#menuMain").shouldBe(Condition.appear);

        $("#menuUsersOpener").hover();
        $("#menuUserAdd").click();

        $("#dataEmail").sendKeys("mail3@testmail.ru");
        $("#dataPassword").sendKeys("testPassword");
        $("#dataName").sendKeys("testUser");
        $("#dataGender").selectOptionContainingText("Женский");
        $("#dataSelect12").click();
        $("#dataSelect22").click();
        $("#dataSend").click();

        $(".uk-modal-body").shouldHave(Condition.text("Данные добавлены."));
        $(".uk-modal-close").click();

        $("#menuUsersOpener").hover();
        $("#menuUsers").click();

        //Это плохая проверка, так как нет защиты от перепутывания столбцов местами.
        $$("tr").filter(Condition.text("mail3@testmail.ru")).first()
                .shouldHave(Condition.text("testUser"))
                .shouldHave(Condition.text("Женский"))
                .shouldHave(Condition.text("1.2"))
                .shouldHave(Condition.text("2.1"))
                .shouldHave(Condition.text("2.2"))
                .shouldNotHave(Condition.text("2.3"));

        WebDriverRunner.closeWebDriver();
    }

    @Test
    void addUserAndCheckVars() {
        open("https://ui-app-for-autotest.herokuapp.com/");
        $("#loginEmail").sendKeys("test@protei.ru");
        $("#loginPassword").sendKeys("test");
        $("#authButton").click();
        $("#menuMain").shouldBe(Condition.appear);

        $("#menuUsersOpener").hover();
        $("#menuUserAdd").click();

        $("#dataEmail").sendKeys("mail5@testmail.ru");
        $("#dataPassword").sendKeys("testPassword");
        $("#dataName").sendKeys("testUser");
        $("#dataGender").selectOptionContainingText("Женский");
        $("#dataSelect12").click();
        $("#dataSelect22").click();
        $("#dataSend").click();

        $(".uk-modal-body").shouldHave(Condition.text("Данные добавлены."));
        $(".uk-modal-close").click();

        $("#menuAuth").click();
        $("#loginEmail").sendKeys("mail5@testmail.ru");
        $("#loginPassword").sendKeys("testPassword");
        $("#authButton").click();
        $("#menuMain").shouldBe(Condition.appear);
        $("#menuMore").click();

        $$(".uk-card-title")
                .shouldHave(CollectionCondition.itemWithText("Вариант 1.2"))
                .shouldHave(CollectionCondition.itemWithText("Вариант 2.1"))
                .shouldHave(CollectionCondition.itemWithText("Вариант 2.2"))
                .shouldHave(CollectionCondition.size(3));

        WebDriverRunner.closeWebDriver();
    }
}
