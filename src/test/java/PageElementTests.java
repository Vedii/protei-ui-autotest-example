import io.qameta.allure.Allure;
import org.testng.annotations.Test;
import ru.protei.models.User;
import ru.protei.pageElement.steps.BaseTest;
import ru.protei.testData.Users;

import static io.qameta.allure.util.ResultsUtils.createTmsLink;
import static ru.protei.testData.Users.admin;
import static ru.protei.testData.Users.getUserRandomData;

public class PageElementTests extends BaseTest {
    /**
     * Класс с тестами на архитектуре "Page Element".
     * Внимание! В боевом проекте не надо складывать все тесты в кучу.
     * Лучше сделать директории под макро-функционал (например, тут - Авторизация, Пользователи)
     * в которые добавить классы с отдельными фичами (например, позитивные и негативные тесты авторизации,
     * создание и просмотр пользователей, пользователь с урезанными правами)
     *
     * По-хорошему надо еще весь "обычный" текст убрать в файл с локалью, но этого не было в статье(
     */

    //Тест без степов
    @Test
    public void authAsAdmin() {
        baseRouter
                .authPage().email.fill(admin.getMail())
                .authPage().password.fill(admin.getPassword())
                .authPage().enter.click()
                .mainPage().logoutButton.shouldExist();
    }

    //Тот же тест со степами
    @Test
    public void authAsAdminIsSuccessful() {
        authSteps
                .fillEmail(admin.getMail())
                .fillPassword(admin.getPassword())
                .clickEnter();
        mainMenuSteps
                .shouldExistLogout();
    }

    @Test
    void addUser() {
        //Arrange
        User userForTest = getUserRandomData();
        authSteps
                .login(admin);
        mainMenuSteps
                .hoverUsersOpener()
                .clickAddUserMenu();
        //Act
        usersSteps
                .fillEmail(userForTest.getMail())
                .fillPassword(userForTest.getPassword())
                .fillName(userForTest.getName())
                .selectGender(userForTest.getGender().getVisibleText())
                .selectVar11(userForTest.isCheck11())
                .selectVar12(userForTest.isCheck12())
                .selectVar21(userForTest.isCheck21())
                .selectVar22(userForTest.isCheck22())
                .selectVar23(userForTest.isCheck23())
                .clickSave()
        //Assert
                .checkTextSavePopup("Данные добавлены.");
    }

    @Test
    void addUserAndCheckInTable() {
        User userForTest = getUserRandomData();
        authSteps
                .login(admin);
        mainMenuSteps
                .hoverUsersOpener()
                .clickAddUserMenu();
        //Act
        usersSteps
                .addUserViaUI(userForTest);
        //плохо, нужно сохранить создание пользователя "на верхнем уровне", так как в этом тесте проверяется отображение после добавления, а не отображение
        //теста на любое отображение было бы достаточно только если в предыдущий тест добавить проверку сохраненных параметров, а не просто успешный алерт
        mainMenuSteps
                .hoverUsersOpener()
                .clickUsersMenu();
        //Assert
        usersSteps
                .checkEntityIsPresentUsersTable("E-Mail", userForTest.getMail())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Имя", userForTest.getName())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Пол", userForTest.getGender().getVisibleText())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Выбор 1", userForTest.getFirstChoice())
                .checkInfoFromFieldUsersTable("E-Mail", userForTest.getMail(), "Выбор 2", userForTest.getSecondChoice());
    }

    @Test
    void checkUserVars() {
        //Arrange
        User userForTest = getUserRandomData();
        // Проверка корректности сохранения полей уже есть, этот тест проверяет отображение вариантов из-под юзера, поэтому не важно, как юзер создан
        usersSteps.createUser(userForTest);
        authSteps.login(userForTest);
        //Act
        mainMenuSteps
                .clickVariantsMenu();
        //Assert
        variantsSteps
                .checkAllVariantsArePresent(userForTest.getVars())
                .checkVariantsCount(userForTest.getVarsCount());
        //Cleanup
        usersSteps.deleteUser(userForTest);
    }

    @Test(dataProviderClass = Users.class, dataProvider = "usersWithDifferentVars")
    void checkUserDifferentVars(User userForTest) {
        //Arrange
        usersSteps.createUser(userForTest);
        authSteps.login(userForTest);
        //Act
        mainMenuSteps
                .clickVariantsMenu();
        //Assert
        variantsSteps
                .checkAllVariantsArePresent(userForTest.getVars())
                .checkVariantsCount(userForTest.getVarsCount());
        //Cleanup
        usersSteps.deleteUser(userForTest);
    }

    //Bonus: how to add TMSLink for data-driven cases (if U know better method, pls, tell us)
    @Test(dataProviderClass = Users.class, dataProvider = "usersAndCases")
    void checkUserDifferentVarsWithAllureTMSLinks(Users.CaseAndUser caseAndUser) {
        String tmsLink = caseAndUser.getCaseNumber();
        User userForTest = caseAndUser.getUser();
        Allure.getLifecycle().updateTestCase(testResult -> testResult.setName(testResult.getName() + " " + userForTest.getVars()));
        Allure.tms(tmsLink, createTmsLink(tmsLink).getUrl());

        //Arrange
        usersSteps.createUser(userForTest);
        authSteps.login(userForTest);
        //Act
        mainMenuSteps
                .clickVariantsMenu();
        //Assert
        variantsSteps
                .checkAllVariantsArePresent(userForTest.getVars())
                .checkVariantsCount(userForTest.getVarsCount());
        //Cleanup
        usersSteps.deleteUser(userForTest);
    }
}
