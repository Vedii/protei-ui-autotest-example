package ru.protei.testData;

import io.github.sskorol.core.DataSupplier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.protei.models.User;

import java.util.stream.Stream;

import static ru.protei.utils.InputsGenerator.*;

public class Users {
    public static final User admin = User.builder().mail("test@protei.ru").password("test").build();

    public static User getUserRandomData() {
        User user = User.builder()
                .mail(getRandomEmail())
                .password(getShortLatinStr())
                .name(getShortLatinStr())
                .gender(getRandomFromEnum(User.Gender.class))
                .check11(getRandomBool())
                .check21(getRandomBool())
                .check22(getRandomBool())
                .check23(getRandomBool())
                .build();
        if (!user.isCheck11()) user.setCheck12(true); //business-logic: 11 xor 12 must be selected
        if (user.isCheck11()) user.setCheck12(false); //business-logic: 11 xor 12 must be selected
        return user;
    }

    @DataSupplier(name = "usersWithDifferentVars")
    public Stream<User> usersWithDifferentVars(){
        return Stream.of(
                getUserRandomData().setCheck21(false).setCheck22(false).setCheck23(false),
                getUserRandomData().setCheck21(true).setCheck22(false).setCheck23(false),
                getUserRandomData().setCheck21(false).setCheck22(true).setCheck23(false),
                getUserRandomData().setCheck21(false).setCheck22(false).setCheck23(true),
                getUserRandomData().setCheck21(true).setCheck22(true).setCheck23(false),
                getUserRandomData().setCheck21(true).setCheck22(false).setCheck23(true),
                getUserRandomData().setCheck21(false).setCheck22(true).setCheck23(true),
                getUserRandomData().setCheck21(true).setCheck22(true).setCheck23(true)
        );
    }

    @Getter
    @AllArgsConstructor
    public class CaseAndUser {
        String caseNumber;
        User user;
    }

    @DataSupplier(name = "usersAndCases")
    public Stream<CaseAndUser> usersAndCases(){
        return Stream.of(
                new CaseAndUser("135", getUserRandomData().setCheck21(false).setCheck22(false).setCheck23(false)),
                new CaseAndUser("136", getUserRandomData().setCheck21(true).setCheck22(true).setCheck23(true))
        );
    }



}
