package ru.protei.utils;

import lombok.Getter;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Getter
public class TestConfig {
    String siteUrl;
    Boolean recordVideoOnSelenoid;

    private static String getPropertyHandler(Properties property, String propertyKey, String defaultValue) {
        return System.getProperty(propertyKey) != null
                ? System.getProperty(propertyKey)
                : property.getProperty(propertyKey, defaultValue);
    }

    public TestConfig() {
        try (FileInputStream fis = new FileInputStream("src/main/resources/test.properties")) {
            Properties property = new Properties();
            property.load(fis);
            siteUrl = getPropertyHandler(property, "siteUrl", "http://192.168.126.179:8083");
            recordVideoOnSelenoid = Boolean.parseBoolean(getPropertyHandler(property, "recordVideoOnSelenoid", "true"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
