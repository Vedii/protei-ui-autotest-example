package ru.protei.utils;

import io.restassured.RestAssured;

import static org.hamcrest.CoreMatchers.containsString;

public class ApiDefaultReq {
    public static void create(String endpoint, Object model) {
        RestAssured.given().spec(ApiSettings.testApi())
                .when()
                .body(model)
                .post(endpoint)
                .then().log().all()
                .statusCode(200)
                .body("state",containsString("OK"));
        // Если сервер возвращает созданный объект, можно его десериализовать в этой функции для валидации
        // .extract().as(model.getClass());
    }

    public static void delete(String endpoint, Integer id) {
        RestAssured.given().spec(ApiSettings.testApi())
                .when().delete(endpoint + "/" + id).then().statusCode(200);
    }

}
