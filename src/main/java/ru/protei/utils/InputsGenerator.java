package ru.protei.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class InputsGenerator {

    private static final SecureRandom random = new SecureRandom();
    private static final String ruChars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    private static final String latChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String space = " ";
    private static final String specChars = "«♣☺♂»,«»‘~!@#$%^&*()?>,./\\][/*!—«»,«${}»;—";
    private static final int defaultLength = 77;
    private static final int shortStringLength = 8;

    public static String getTimestamp() {
        return (new SimpleDateFormat("YYMMddHHmmss")).format(new Date());
    }

    public static String getNumAsStr(int From, int To) {
        return Integer.toString(random.nextInt((To - From) + 1) + From);
    }

    public static <T extends Enum<?>> T getRandomFromEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public static <T> T getRandomFromList(List<T> givenList) {
        return givenList.get(random.nextInt(givenList.size()));
    }

    public static boolean getRandomBool() {
        return random.nextBoolean();
    }

    public static String getShortLatinStr() {
        return getLatinStrWithoutSpaces(shortStringLength);
    }

    public static String getCyrillicStrWithoutSpaces() {
        return getCyrillicStrWithoutSpaces(defaultLength);
    }

    public static String getCyrillicStrWithoutSpaces(int count) {
        return RandomStringUtils.random(count, ruChars);
    }

    public static String getCyrillicStrWithSpaces() {
        return getCyrillicStrWithSpaces(defaultLength);
    }

    public static String getCyrillicStrWithSpaces(int count) {
        return RandomStringUtils.random(count, ruChars + space).replace("  ", " ").trim();
    }

    public static String getLatinStrWithoutSpaces() {
        return getLatinStrWithoutSpaces(defaultLength);
    }

    public static String getLatinStrWithoutSpaces(int count) {
        return RandomStringUtils.random(count, latChars);
    }

    public static String getLatinStrWithSpaces() {
        return getLatinStrWithSpaces(defaultLength);
    }

    public static String getLatinStrWithSpaces(int count) {
        return RandomStringUtils.random(count, latChars + space).replace("  ", " ").trim();
    }

    public static String getLatinStrWithSpacesAndTestName(int count, String testName) {
        return (testName.replace("  ", " ").trim() + " " + RandomStringUtils.random(count, latChars).replace("  ", " ").trim());
    }

    public static String getSpecCharStrWithSpaces() {
        return getSpecCharStrWithSpaces(defaultLength);
    }

    public static String getSpecCharStrWithSpaces(int count) {
        return RandomStringUtils.random(defaultLength, specChars);
    }

    public static String getJSInjection() {
        return "alert( 'HI' );";
    }

    public static String getSQLInjection() {
        return "ы'); DROP TABLE Person;--";
    }

    public static String getRandomEmail() { return getShortLatinStr() + "@" + getShortLatinStr() + ".ru";}
    public static String getRandomIP() { return "" + getRandomIntInRange(1,254) + "." + getRandomIntInRange(0,255) + "." + getRandomIntInRange(0,255) + "." + getRandomIntInRange(0,255);}
    public static String getRandomIPWithPort() { return getRandomIP() + ":" + getRandomIntInRange(1,65535);}
    public static String getLatitude() { return removeTrailingZeroes("59" + "." + getRandomIntInRange(50000,62000));} //SPb
    public static String getLongitude() { return removeTrailingZeroes("30" + "." + getRandomIntInRange(19000,40000));} //SPb

    private static String removeLeadingZeroes(String s) {
        return StringUtils.stripStart(s, "0");
    }

    private static String removeTrailingZeroes(String s) {
        return StringUtils.stripEnd(s, "0");
    }

    public static int getRandomIntInRange(int min, int max) {
        if (min > max)
            throw new IllegalArgumentException("max must be greater than min");
        else if (max == min)
            return min;
        return random.nextInt((max - min) + 1) + min;
    }
    public static Long getRandomLongInRange(int min, int max) {
        if (min > max)
            throw new IllegalArgumentException("max must be greater than min");
        else if (max == min)
            return (long) min;
        return (long) (random.nextInt((max - min) + 1) + min);
    }

    public static String getHTMLInjection() {
        return "<table border='1'>" +
                " <caption>Таблица размеров обуви</caption>" +
                " <tr>" +
                " <th>Россия</th>" +
                " <th>Великобритания</th>" +
                " <th>Европа</th>" +
                " <th>Длина ступни, см</th>" +
                " </tr>" +
                " </table>";
    }
}
