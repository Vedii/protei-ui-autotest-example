package ru.protei.pageObject.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import static com.codeborne.selenide.Selenide.page;

public class MainPage {
    @FindBy(how = How.ID, using = "menuAuth")
    private SelenideElement logoutButton;
    @FindBy(how = How.ID, using = "menuMain")
    private SelenideElement mainButton;
    @FindBy(how = How.ID, using = "menuUsersOpener")
    private SelenideElement usersOpener;
    @FindBy(how = How.ID, using = "menuUsers")
    private SelenideElement usersButton;
    @FindBy(how = How.ID, using = "menuUserAdd")
    private SelenideElement addUserButton;
    @FindBy(how = How.ID, using = "menuMore")
    private SelenideElement variantsButton;


    @Step("Logout should exist")
    public MainPage shouldExistLogout() {
        logoutButton.should(Condition.exist);
        return this;
    }

    @Step("Hover users menu opener")
    public MainPage hoverUsersOpener() {
        usersOpener.hover();
        return this;
    }

    @Step("Hover users menu opener")
    public UsersPage complexOpenAddUser() {
        usersOpener.hover();
        addUserButton.click();
        return page(UsersPage.class);
    }

    @Step("Click users menu opener")
    public MainPage clickUsersOpener() {
        usersOpener.click();
        return this;
    }

    @Step("Click users menu button")
    public UsersPage clickUsersMenu() {
        usersButton.click();
        return page(UsersPage.class);
    }

    @Step("Click add user menu button")
    public UsersPage clickAddUserMenu() {
        addUserButton.click();
        return page(UsersPage.class);
    }

    @Step("Click variants menu button")
    public VariantsPage clickVariantsMenu() {
        variantsButton.click();
        return page(VariantsPage.class);
    }
}
