package ru.protei.pageObject.pages;

import static com.codeborne.selenide.Selenide.page;

public class BaseRouter {
    public AuthPage authPage() {return page(AuthPage.class);}
    public MainPage mainPage() {return page(MainPage.class);}
    public UsersPage usersPage() {return page(UsersPage.class);}
    public VariantsPage variantsPage() {return page(VariantsPage.class);}
}
