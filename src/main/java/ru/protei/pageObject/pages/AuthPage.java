package ru.protei.pageObject.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ru.protei.models.User;

import static com.codeborne.selenide.Selenide.page;

public class AuthPage {
    @FindBy(how = How.ID, using = "loginEmail")
    private SelenideElement email;
    @FindBy(how = How.ID, using = "loginPassword")
    private SelenideElement password;
    @FindBy(how = How.ID, using = "authButton")
    private SelenideElement enter;
    @FindBy(how = How.CSS, using = ".uk-alert p")
    private SelenideElement errorText;
    @FindBy(how = How.CSS, using = ".uk-alert-close")
    private SelenideElement errorCloser;

    @Step("Sigh in as {user}")
    public MainPage complexLogin(User user) {
        email.sendKeys(user.getMail());
        password.sendKeys(user.getPassword());
        enter.click();
        return page(MainPage.class);
    }

    @Step("Fill E-mail")
    public AuthPage fillEmail(String text) {
        email.sendKeys(text);
        return this;
    }

    @Step("Fill password")
    public AuthPage fillPassword(String text) {
        password.sendKeys(text);
        return this;
    }

    @Step("Click enter")
    public MainPage clickEnter() {
        enter.click();
        return page(MainPage.class);
    }

    @Step("Enter should exist")
    public AuthPage shouldExistEnter() {
        enter.should(Condition.exist);
        return this;
    }
}
