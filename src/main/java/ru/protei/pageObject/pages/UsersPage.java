package ru.protei.pageObject.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ru.protei.models.User;

import static com.codeborne.selenide.Selenide.$;

public class UsersPage {
    @FindBy(how = How.TAG_NAME, using = "table")
    private SelenideElement table;
    @FindBy(how = How.TAG_NAME, using = "button")
    private SelenideElement addUserButton;

    @FindBy(how = How.ID, using = "dataEmail")
    private SelenideElement email;
    @FindBy(how = How.ID, using = "dataPassword")
    private SelenideElement password;
    @FindBy(how = How.ID, using = "dataName")
    private SelenideElement name;
    @FindBy(how = How.ID, using = "dataGender")
    private SelenideElement gender;
    @FindBy(how = How.ID, using = "dataSelect11")
    private SelenideElement var11;
    @FindBy(how = How.ID, using = "dataSelect12")
    private SelenideElement var12;
    @FindBy(how = How.ID, using = "dataSelect21")
    private SelenideElement var21;
    @FindBy(how = How.ID, using = "dataSelect22")
    private SelenideElement var22;
    @FindBy(how = How.ID, using = "dataSelect23")
    private SelenideElement var23;
    @FindBy(how = How.ID, using = "dataSend")
    private SelenideElement save;

    @FindBy(how = How.CSS, using = ".uk-alert p")
    private SelenideElement errorText;
    @FindBy(how = How.CSS, using = ".uk-alert-close")
    private SelenideElement errorClose;
    @FindBy(how = How.CSS, using = ".uk-modal-body")
    private SelenideElement saveText;
    @FindBy(how = How.CSS, using = ".uk-modal-close")
    private SelenideElement saveClose;

    @Step("Complex add user")
    public UsersPage complexAddUser(User user) {
        email.sendKeys(user.getMail());
        password.sendKeys(user.getPassword());
        name.sendKeys(user.getName());
        gender.selectOption(user.getGender().getVisibleText());
        set(var11, user.isCheck11());
        set(var12, user.isCheck12());
        set(var21, user.isCheck21());
        set(var22, user.isCheck22());
        set(var23, user.isCheck23());
        save.click();
        return this;
    }

    @Step("Check and close successful alert")
    public UsersPage checkAndCloseSuccessfulAlert() {
        saveText.shouldHave(Condition.text("Данные добавлены."));
        saveClose.click();
        return this;
    }

    @Step("Fill user Email")
    public UsersPage sendKeysEmail(String text) {
        email.sendKeys(text);
        return this;
    }

    @Step("Fill user Password")
    public UsersPage sendKeysPassword(String text) {
        password.sendKeys(text);
        return this;
    }

    @Step("Fill user Name")
    public UsersPage sendKeysName(String text) {
        name.sendKeys(text);
        return this;
    }

    @Step("Select user Gender")
    public UsersPage selectGender(String text) {
        gender.selectOption(text);
        return this;
    }

    @Step("Select user variant 1.1")
    public UsersPage selectVar11(boolean flag) {
        set(var11, flag);
        return this;
    }

    @Step("Select user variant 1.2")
    public UsersPage selectVar12(boolean flag) {
        set(var12, flag);
        return this;
    }

    @Step("Select user variant 2.1")
    public UsersPage selectVar21(boolean flag) {
        set(var21, flag);
        return this;
    }

    @Step("Select user variant 2.2")
    public UsersPage selectVar22(boolean flag) {
        set(var22, flag);
        return this;
    }

    @Step("Select user variant 2.3")
    public UsersPage selectVar23(boolean flag) {
        set(var23, flag);
        return this;
    }

    @Step("Click save")
    public UsersPage clickSave() {
        save.click();
        return this;
    }

    @Step("Check text in error")
    public UsersPage checkTextError(String text) {
        errorText.shouldHave(Condition.text(text));
        return this;
    }

    @Step("Close error")
    public UsersPage closeError() {
        errorClose.click();
        return this;
    }

    @Step("Check text in savePopup")
    public UsersPage checkTextSavePopup(String text) {
        saveText.shouldHave(Condition.text(text));
        return this;
    }

    @Step("Close savePopup")
    public UsersPage closeSavePopup() {
        saveClose.click();
        return this;
    }

    @Step("Check entity is present in table")
    public UsersPage checkEntityIsPresentUsersTable(String columnName, String entityName) {
        table.$x((".//td[position()="+getNumberOfColumn(columnName)+" and normalize-space(.)='"+entityName+"']")).should(Condition.exist);
        return this;
    }

    @Step("Check entity is not present in table")
    public UsersPage checkEntityIsNotPresentUsersTable(String columnName, String entityName) {
        table.$x((".//td[position()=" + getNumberOfColumn(columnName) + " and normalize-space(.)='" + entityName + "']")).should(Condition.not(Condition.exist));
        return this;
    }

    @Step("Check info from field in table")
    public UsersPage checkInfoFromFieldUsersTable(String columnName, String entityName, String requiredColumnName, String expectedText) {
        SelenideElement row = table.$x((".//td[position()="+getNumberOfColumn(columnName)+" and normalize-space(.)='"+entityName+"']")).parent();
        SelenideElement required = row.$x("./td[position()="+getNumberOfColumn(requiredColumnName)+"]");
        if (expectedText.equals(""))
            required.shouldHave(Condition.exactText(""));
        else
            required.shouldHave(Condition.text(expectedText));
        return this;
    }

    private int getNumberOfColumn(String columnName) {
        table.$$("tr").shouldBe(CollectionCondition.sizeGreaterThan(0));
        return $(table).$$("th").indexOf($(table).$(By.xpath(".//th[normalize-space(.)='"+columnName+"']")))+1;
    }

    private void set(SelenideElement checkbox, boolean flag) {
        if (flag) {
            if (!checkbox.isSelected()) checkbox.click();
        } else {
            if (checkbox.isSelected()) checkbox.click();
        }
    }

}
