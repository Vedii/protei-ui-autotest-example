package ru.protei.pageObject.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.ArrayList;

public class VariantsPage {
    @FindBy(how = How.CSS, using = ".uk-card")
    private ElementsCollection variants;


    @Step("Check variant {title} is not present")
    public VariantsPage checkVariantIsNotPresent(String title) {
        variants.filterBy(Condition.text(title)).shouldBe(CollectionCondition.empty);
        return this;
    }

    @Step("Check variant {title} presents")
    public VariantsPage checkVariantIsPresent(String title) {
        variants.filterBy(Condition.text(title)).shouldBe(CollectionCondition.size(1));
        return this;
    }

    @Step("Check all variants present")
    public VariantsPage checkAllVariantsArePresent(ArrayList<String> list) {
        for (String title: list)
        variants.filterBy(Condition.text(title)).shouldBe(CollectionCondition.size(1));
        return this;
    }

    @Step("Check none variants present")
    public VariantsPage checkVariantsEmpty() {
        variants.shouldBe(CollectionCondition.empty);
        return this;
    }

    @Step("Check variants count")
    public VariantsPage checkVariantsCount(int count) {
        variants.shouldBe(CollectionCondition.size(count));
        return this;
    }
}
