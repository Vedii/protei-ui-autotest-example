package ru.protei.pageObject.steps;

import io.qameta.allure.Step;
import ru.protei.models.User;

import static ru.protei.utils.ApiDefaultReq.create;
import static ru.protei.utils.ApiDefaultReq.delete;

public class UsersSteps {

    @Step("Create user")
    public UsersSteps createUser(User user) {
//        Если сервер возвращает объект, тут из него можно достать и проверить нужные поля, например
//        User createdUser = (User)create(user.getEndpoint(), user);
//        assertEquals(user, createdUser);
        create(User.endpoint, user);
        return this;
    }

    @Step("Delete user")
    public UsersSteps deleteUser(User user) {
        if (user.getId() == null) {
            return this;
            //тут можно выполнить запрос на все сущности и отфильтровать в ответе нужного пользователя по уникальному полю
            //но демо-сайт в такое не умеет)
        }
        delete(User.endpoint, user.getId());
        return this;
    }

    // Этот класс можно продолжить, оборачивая каждый метод из пейджа в степ. Но без автогенерации я на это не готова даже ради примера)

}
