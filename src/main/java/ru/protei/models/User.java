package ru.protei.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.StringJoiner;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    @JsonIgnore
    public static final String endpoint = "/user";

    private Integer id;
    private String mail;
    private String name;
    private String password;
    private Gender gender;

    private boolean check11;
    private boolean check12;
    private boolean check21;
    private boolean check22;
    private boolean check23;

    public enum Gender {
        MALE,
        FEMALE;

        public String getVisibleText() {
            switch (this) {
                case MALE:
                    return "Мужской";
                case FEMALE:
                    return "Женский";
            }
            return "";
        }
    }

    @JsonIgnore
    public ArrayList<String> getVars() {
        ArrayList<String> output = new ArrayList<>();
        if (this.check11) output.add("Вариант 1.1");
        if (this.check12) output.add("Вариант 1.2");
        if (this.check21) output.add("Вариант 2.1");
        if (this.check22) output.add("Вариант 2.2");
        if (this.check23) output.add("Вариант 2.3");
        return output;
    }

    @JsonIgnore
    public int getVarsCount() {
        ArrayList<String> output = this.getVars();
        return output.size();
    }

    @JsonIgnore
    public String getFirstChoice() {
        if (this.isCheck11()) return "1.1";
        else return "1.2";
    }

    @JsonIgnore
    public String getSecondChoice() {
        StringJoiner joiner = new StringJoiner(", ");
        if (this.isCheck21()) joiner.add("2.1");
        if (this.isCheck22()) joiner.add("2.2");
        if (this.isCheck23()) joiner.add("2.3");
        return joiner.toString();
    }

}