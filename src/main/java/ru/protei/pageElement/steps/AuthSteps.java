package ru.protei.pageElement.steps;

import io.qameta.allure.Step;
import ru.protei.models.User;

public class AuthSteps extends BaseSteps{

    @Step("Sigh in as {user}")
    public BaseSteps login(User user) {
        baseRouter.authPage().email.fill(user.getMail())
                .authPage().password.fill(user.getPassword())
                .authPage().enter.click()
                .mainPage().logoutButton.shouldExist();
        return this;
    }

    @Step("Fill E-mail")
    public AuthSteps fillEmail(String email) {
        baseRouter.authPage().email.fill(email);
        return this;
    }

    @Step("Fill password")
    public AuthSteps fillPassword(String password) {
        baseRouter.authPage().password.fill(password);
        return this;
    }

    @Step("Click enter")
    public AuthSteps clickEnter() {
        baseRouter.authPage().enter.click();
        return this;
    }

    @Step("Enter should exist")
    public AuthSteps shouldExistEnter() {
        baseRouter.authPage().enter.shouldExist();
        return this;
    }

    @Step("Logout")
    public AuthSteps logout() {
        baseRouter.mainPage().logoutButton.click()
                .authPage().enter.shouldExist();
        return this;
    }

}
