package ru.protei.pageElement.steps;

import io.qameta.allure.Step;
import ru.protei.models.User;

import static ru.protei.utils.ApiDefaultReq.create;
import static ru.protei.utils.ApiDefaultReq.delete;

public class UsersSteps extends BaseSteps{

    @Step("Create user")
    public UsersSteps createUser(User user) {
//        Если сервер возвращает объект, тут из него можно достать и проверить нужные поля, например
//        User createdUser = (User)create(user.getEndpoint(), user);
//        assertEquals(user, createdUser);
        create(User.endpoint, user);
        return this;
    }

    @Step("Delete user")
    public UsersSteps deleteUser(User user) {
        if (user.getId() == null) {
            return this;
            //тут можно выполнить запрос на все сущности и отфильтровать в ответе нужного пользователя по уникальному полю
            //но демо-сайт в такое не умеет)
        }
        delete(User.endpoint, user.getId());
        return this;
    }

    @Step("Add user via UI")
    public UsersSteps addUserViaUI(User user) {
        this                
                .fillEmail(user.getMail())
                .fillPassword(user.getPassword())
                .fillName(user.getName())
                .selectGender(user.getGender().getVisibleText())
                .selectVar11(user.isCheck11())
                .selectVar12(user.isCheck12())
                .selectVar21(user.isCheck21())
                .selectVar22(user.isCheck22())
                .selectVar23(user.isCheck23())
                .clickSave()
                .checkTextSavePopup("Данные добавлены.")
                .closeSavePopup();
        return this;
    }

    @Step("Fill user Email")
    public UsersSteps fillEmail(String text) {
        baseRouter
                .usersPage().email.fill(text);
        return this;
    }

    @Step("Fill user Password")
    public UsersSteps fillPassword(String text) {
        baseRouter
                .usersPage().password.fill(text);
        return this;
    }

    @Step("Fill user Name")
    public UsersSteps fillName(String text) {
        baseRouter
                .usersPage().name.fill(text);
        return this;
    }

    @Step("Select user Gender")
    public UsersSteps selectGender(String text) {
        baseRouter
                .usersPage().gender.choose(text);
        return this;
    }

    @Step("Select user variant 1.1")
    public UsersSteps selectVar11(boolean flag) {
        baseRouter
                .usersPage().var11.set(flag);
        return this;
    }

    @Step("Select user variant 1.2")
    public UsersSteps selectVar12(boolean flag) {
        baseRouter
                .usersPage().var12.set(flag);
        return this;
    }

    @Step("Select user variant 2.1")
    public UsersSteps selectVar21(boolean flag) {
        baseRouter
                .usersPage().var21.set(flag);
        return this;
    }

    @Step("Select user variant 2.2")
    public UsersSteps selectVar22(boolean flag) {
        baseRouter
                .usersPage().var22.set(flag);
        return this;
    }

    @Step("Select user variant 2.3")
    public UsersSteps selectVar23(boolean flag) {
        baseRouter
                .usersPage().var23.set(flag);
        return this;
    }

    @Step("Click save")
    public UsersSteps clickSave() {
        baseRouter
                .usersPage().save.click();
        return this;
    }

    @Step("Check text in error")
    public UsersSteps checkTextError(String text) {
        baseRouter
                .usersPage().errorPopup.checkText(text);
        return this;
    }

    @Step("Close error")
    public UsersSteps closeError() {
        baseRouter
                .usersPage().errorPopup.close();
        return this;
    }

    @Step("Check text in savePopup")
    public UsersSteps checkTextSavePopup(String text) {
        baseRouter
                .usersPage().savePopup.checkText(text);
        return this;
    }

    @Step("Close savePopup")
    public UsersSteps closeSavePopup() {
        baseRouter
                .usersPage().savePopup.close();
        return this;
    }

    @Step("Check entity is present in table")
    public UsersSteps checkEntityIsPresentUsersTable(String columnName, String entityName) {
        baseRouter
                .usersPage().usersTable.checkEntityIsPresent(columnName, entityName);
        return this;
    }

    @Step("Check entity is not present in table")
    public UsersSteps checkEntityIsNotPresentUsersTable(String columnName, String entityName) {
        baseRouter
                .usersPage().usersTable.checkEntityIsNotPresent(columnName, entityName);
        return this;
    }

    @Step("Check info from field in table")
    public UsersSteps checkInfoFromFieldUsersTable(String columnName, String entityName, String requiredColumnName, String expectedText) {
        baseRouter
                .usersPage().usersTable.checkInfoFromField(columnName, entityName, requiredColumnName, expectedText);
        return this;
    }

}
