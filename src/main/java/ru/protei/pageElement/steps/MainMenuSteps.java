package ru.protei.pageElement.steps;

import io.qameta.allure.Step;

public class MainMenuSteps extends BaseSteps{

    @Step("Logout should exist")
    public MainMenuSteps shouldExistLogout() {
        baseRouter.mainPage().logoutButton.shouldExist();
        return this;
    }

    @Step("Hover users menu opener")
    public MainMenuSteps hoverUsersOpener() {
        baseRouter.mainPage().usersOpener.hover();
        return this;
    }

    @Step("Click users menu opener")
    public MainMenuSteps clickUsersOpener() {
        baseRouter.mainPage().usersOpener.click();
        return this;
    }

    @Step("Click users menu button")
    public MainMenuSteps clickUsersMenu() {
        baseRouter.mainPage().usersButton.click();
        return this;
    }

    @Step("Click add user menu button")
    public MainMenuSteps clickAddUserMenu() {
        baseRouter.mainPage().addUserButton.click();
        return this;
    }

    @Step("Click variants menu button")
    public MainMenuSteps clickVariantsMenu() {
        baseRouter.mainPage().variantsButton.click();
        return this;
    }
}
