package ru.protei.pageElement.steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import io.restassured.RestAssured;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import ru.protei.pageElement.pages.BaseRouter;
import ru.protei.utils.TestConfig;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

public class BaseTest {
    public static TestConfig testConfig = new TestConfig();
    public BaseRouter baseRouter = new BaseRouter(); // только для теста без степов, при наличии степов не нужен
    protected AuthSteps authSteps = new AuthSteps();
    protected MainMenuSteps mainMenuSteps = new MainMenuSteps();
    protected UsersSteps usersSteps = new UsersSteps();
    protected VariantsSteps variantsSteps = new VariantsSteps();

    @BeforeClass
    public void setUp() {
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeMethod
    protected void openSite() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        Configuration.browserSize = "1920x1070";
        closeWebDriver();
        open(testConfig.getSiteUrl());
    }

    @AfterMethod
    protected void reportAndCloseBrowser(ITestResult result) {
        // bonus for selenoid ^^
        String sessionID = ((RemoteWebDriver) WebDriverRunner.getWebDriver())
                .getSessionId()
                .toString();
        videoInHtml(sessionID);
        closeWebDriver();
    }


    @Attachment(value = "Video HTML", type = "text/html", fileExtension = ".html")
    public static String videoInHtml(String sessionId) {
        if (testConfig.getRecordVideoOnSelenoid()) {
            return "<html><body>" +
                    "<video width='100%' height='100%' controls autoplay><source src='"
                    + "http://selenoid/video/" + sessionId + ".mp4"
                    + "' type='video/mp4'>" +
                    "</video></body></html>";
        }
        return "<html><body>" +
                "<h1 style=\"color: #5e9ca0;\" data-mce-style=\"color: #5e9ca0;\">Видео было выключено в настройках билда" +
                "</h1></body></html>";
    }

}
