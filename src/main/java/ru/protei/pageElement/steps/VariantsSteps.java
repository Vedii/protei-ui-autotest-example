package ru.protei.pageElement.steps;

import io.qameta.allure.Step;

import java.util.ArrayList;

public class VariantsSteps extends BaseSteps{

    @Step("Check variant {title} is not present")
    public VariantsSteps checkVariantIsNotPresent(String title) {
        baseRouter.variantsPage().variants.checkVariantIsNotPresent(title);
        return this;
    }

    @Step("Check variant {title} presents")
    public VariantsSteps checkVariantIsPresent(String title) {
        baseRouter.variantsPage().variants.checkVariantIsPresent(title);
        return this;
    }

    @Step("Check all variants present")
    public VariantsSteps checkAllVariantsArePresent(ArrayList<String> list) {
        baseRouter.variantsPage().variants.checkAllVariantsArePresent(list);
        return this;
    }

    @Step("Check none variants present")
    public VariantsSteps checkVariantsEmpty() {
        baseRouter.variantsPage().variants.checkVariantsEmpty();
        return this;
    }

    @Step("Check variants count")
    public VariantsSteps checkVariantsCount(int count) {
        baseRouter.variantsPage().variants.checkVariantsCount(count);
        return this;
    }
}
