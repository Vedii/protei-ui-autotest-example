package ru.protei.pageElement.widgets;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.$$;

public class Variants extends BaseRouter{

    private final By variant = By.cssSelector(".uk-card");

    public BaseRouter checkVariantIsPresent(String title) {
        $$(variant).filterBy(Condition.text(title)).shouldBe(CollectionCondition.size(1));
        return this;
    }

    public BaseRouter checkAllVariantsArePresent(ArrayList<String> list) {
        for (String title: list)
            $$(variant).filterBy(Condition.text(title)).shouldBe(CollectionCondition.size(1));
        return this;
    }

    public BaseRouter checkVariantIsNotPresent(String title) {
        $$(variant).filterBy(Condition.text(title)).shouldBe(CollectionCondition.empty);
        return this;
    }

    public BaseRouter checkVariantsEmpty() {
        $$(variant).shouldBe(CollectionCondition.empty);
        return this;
    }

    public BaseRouter checkVariantsCount(int count) {
        $$(variant).shouldBe(CollectionCondition.size(count));
        return this;
    }

    public BaseRouter hoverVariant(String title) {
        $$(variant).filterBy(Condition.text(title)).first().hover();
        return this;
    }

}
