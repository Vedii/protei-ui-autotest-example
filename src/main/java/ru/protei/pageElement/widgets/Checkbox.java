package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class Checkbox extends BaseRouter {
    private By locator;

    public Checkbox(By by) {
        locator =by;
    }

    public boolean isSelected() {
        return $(locator).isSelected();
    }

    public BaseRouter set(boolean flag) {
        if (flag) {
            if (!isSelected()) $(locator).click();
        } else {
            if (isSelected()) $(locator).click();
        }
        return this;
    }

    public BaseRouter setTrue() {
        if (!isSelected()) {$(locator).click();}
        return this;
    }

    public BaseRouter checkTrue() {
        $(locator).shouldBe(Condition.selected);
        return this;
    }

    public BaseRouter setFalse() {
        if (isSelected()) $(locator).click();
        return this;
    }

    public BaseRouter checkFalse() {
        $(locator).shouldBe(Condition.not(Condition.selected));
        return this;
    }

    public BaseRouter clickBox() {
        $(locator).click();
        return this;
    }

    public BaseRouter checkIsUnclickable() {
        $(locator).shouldBe(Condition.disabled);
        return this;
    }
}
