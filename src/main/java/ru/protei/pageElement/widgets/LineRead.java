package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;


public class LineRead extends BaseRouter {

    private By locator;

    public LineRead(By locator) {
        this.locator = locator;
    }

    public String getText() {
        return $(locator).shouldBe(Condition.visible).getText();
    }

    public BaseRouter checkText(String expected) {
        $(locator).shouldHave(Condition.text(expected));
        return this;
    }
}