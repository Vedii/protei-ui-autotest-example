package ru.protei.pageElement.widgets;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class Table extends BaseRouter {

    private final By table = By.tagName("table");
    private final By headers = By.tagName("th");
    private final By rows = By.tagName("tr");
    private final By fields = By.tagName("td");

    public int getNumberOfColumn(String columnName) {
        $(table).$$(rows).shouldBe(CollectionCondition.sizeGreaterThan(0));
        int column_number = $(table).$$(headers).indexOf($(table).$(By.xpath(".//th[normalize-space(.)='"+columnName+"']")))+1;
        return column_number;
    }

    public BaseRouter checkEntityIsPresent (String columnName, String entityName) {
        $(table).$x((".//td[position()="+getNumberOfColumn(columnName)+" and normalize-space(.)='"+entityName+"']")).should(Condition.exist);
        return this;
    }

    public BaseRouter checkEntityIsNotPresent (String columnName, String entityName) {
        if (!$(table).exists()) return this;
        if (!$(table).isDisplayed()) return this;
        try {
            $(table).$x((".//td[position()=" + getNumberOfColumn(columnName) + " and normalize-space(.)='" + entityName + "']")).should(Condition.not(Condition.exist));
        }
        catch (org.openqa.selenium.NoSuchElementException e) {return this;}
        return this;
    }

    public String getInfoFromField (String columnName, String entityName, String requiredColumn) {
        return $(table).$x((".//td[position()="+getNumberOfColumn(columnName)+" and normalize-space(.)='"+entityName+"']"))
                .$x("./../td[position()="+getNumberOfColumn(requiredColumn)+"]").getText();
    }

    public BaseRouter checkInfoFromField (String columnName, String entityName, String requiredColumn, String expected) {
        SelenideElement row = $(table).$x((".//td[position()="+getNumberOfColumn(columnName)+" and normalize-space(.)='"+entityName+"']")).parent();
        SelenideElement required = row.$x("./td[position()="+getNumberOfColumn(requiredColumn)+"]");
        if (expected.equals(""))
            required.shouldHave(Condition.exactText(""));
        else
            required.shouldHave(Condition.text(expected));
        return this;
    }


}
