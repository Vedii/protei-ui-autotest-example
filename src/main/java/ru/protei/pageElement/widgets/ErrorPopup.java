package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class ErrorPopup extends BaseRouter {

    private By closer = By.cssSelector(".uk-alert-close");
    private By text = By.cssSelector(".uk-alert p");

    public BaseRouter close () {
        $(closer).click();
        return this;
    }

    public String getText() {
        return $(text).getText();
    }

    public String getTextIfExist () {
        try {
            return $(text).getText();
        } catch (com.codeborne.selenide.ex.ElementNotFound e) {
            return "no err";
        }
    }

    public BaseRouter checkText(String expected) {
        $(text).shouldHave(Condition.text(expected));
        return this;
    }
}
