package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class ModalPopup extends BaseRouter {

    private By closer = By.cssSelector(".uk-modal-close");
    private By text = By.cssSelector(".uk-modal-body");

    public BaseRouter close () {
        $(closer).click();
        return this;
    }

    public BaseRouter checkText(String expected) {
        $(text).shouldHave(Condition.text(expected));
        return this;
    }
}
