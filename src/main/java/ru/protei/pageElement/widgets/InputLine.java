package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import ru.protei.pageElement.pages.BaseRouter;

import java.awt.*;
import java.awt.datatransfer.StringSelection;

import static com.codeborne.selenide.Selenide.$;

@Log
public class InputLine extends BaseRouter{
        private By locator;

        public InputLine(By locator) {
            this.locator = locator;
        }

        public BaseRouter clear() {
            $(locator).clear();
//        backspaces to get error
            $(locator).sendKeys("a");
            $(locator).sendKeys(Keys.BACK_SPACE);
            return this;
        }

        public BaseRouter fill(String text) {
            $(locator).sendKeys(text);
            return this;
        }

        public BaseRouter insert(String text) {
            // 'ctrl+c' for local browser
            try {
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(text), null);
            }
            catch (HeadlessException e){log.info("Seems not local machine, no X11");}
            // 'ctrl+v'
            $(locator).sendKeys(Keys.CONTROL + "v");
            return this;
        }

        public BaseRouter clearWithFill(String text) {
            $(locator).clear();
            $(locator).sendKeys(text);
            return this;
        }

        public BaseRouter fillWithEnter(String text) {
            $(locator).sendKeys(text);
            $(locator).pressEnter();
            return this;
        }

        public String getText() {
            return $(locator).getAttribute("value");
        }

        public BaseRouter checkText(String expected) {
            String current = $(locator).getAttribute("value");
            if (!current.equals(expected)) throw new AssertionError("Line content is incorrect! " +
                    "Expected '" + expected + "', get '" + current + "'!");
            return this;
        }
        public BaseRouter actionsFill(String text) {
            // useful for js-filling fields
            Action action = new Actions(WebDriverRunner.getWebDriver())
                    .moveToElement($(locator).shouldBe(Condition.exist).toWebElement())
                    .click()
                    .sendKeys(text)
                    .build();
            action.perform();
            return this;
        }
        public BaseRouter actionsClear() {
            // useful for js-filling fields
            Action action = new Actions(WebDriverRunner.getWebDriver())
                    .moveToElement($(locator).shouldBe(Condition.exist).toWebElement())
                    .click()
                    .sendKeys("a")
                    .sendKeys(Keys.BACK_SPACE)
                    .build();
            action.perform();
            return this;
        }

}
