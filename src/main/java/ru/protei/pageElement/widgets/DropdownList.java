package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class DropdownList extends BaseRouter {

    private By locator;

    public DropdownList(By locator) {
        this.locator = locator;
    }

    public BaseRouter choose (String variant) {
        $(locator).selectOption(variant);
        return this;
    }

    public BaseRouter checkText(String expected) {
        $(locator).shouldHave(Condition.text(expected));
        return this;
    }
}
