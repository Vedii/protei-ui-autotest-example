package ru.protei.pageElement.widgets;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import ru.protei.pageElement.pages.BaseRouter;

import static com.codeborne.selenide.Selenide.$;

public class Button extends BaseRouter {

    private By locator;

    public Button(By buttonLocator) {
        locator = buttonLocator;
    }

    public BaseRouter click() {
        $(locator).shouldBe(Condition.enabled).click();
        return this;
    }

    public BaseRouter hover() {
        $(locator).hover();
        return this;
    }

    public String getText() {
        return $(locator).text();
    }

    public BaseRouter checkText(String expected) {
        $(locator).shouldHave(Condition.text(expected));
        return this;
    }

    public BaseRouter waitDisappear(int seconds) {
        try {
            Configuration.timeout = 1000 * seconds;
            $(locator).should(Condition.disappear);
        }
        finally {
            Configuration.timeout = 3000;
        }
        return this;
    }

    public BaseRouter shouldExist() {
        $(locator).should(Condition.exist);
        return this;
    }
    public BaseRouter shouldNotExist() {
        $(locator).should(Condition.not(Condition.exist));
        return this;
    }
}
