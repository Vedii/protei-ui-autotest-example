package ru.protei.pageElement.pages;

import org.openqa.selenium.By;
import ru.protei.pageElement.widgets.Button;

public class MainPage {
    public Button logoutButton = new Button(By.id("menuAuth"));
    public Button mainButton = new Button(By.id("menuMain"));
    public Button usersOpener = new Button(By.id("menuUsersOpener"));
    public Button usersButton = new Button(By.id("menuUsers"));
    public Button addUserButton = new Button(By.id("menuUserAdd"));
    public Button variantsButton = new Button(By.id("menuMore"));
}
