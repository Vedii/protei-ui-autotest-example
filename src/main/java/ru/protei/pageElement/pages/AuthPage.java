package ru.protei.pageElement.pages;

import org.openqa.selenium.By;
import ru.protei.pageElement.widgets.Button;
import ru.protei.pageElement.widgets.ErrorPopup;
import ru.protei.pageElement.widgets.InputLine;

public class AuthPage {
    public InputLine email = new InputLine(By.id("loginEmail"));
    public InputLine password = new InputLine(By.id("loginPassword"));
    public Button enter = new Button(By.id("authButton"));
    public ErrorPopup error = new ErrorPopup();
}
